﻿using Modul2Test_Branislav_Jovanovic.Models;
using Modul2Test_Branislav_Jovanovic.Repository;
using Modul2Test_Branislav_Jovanovic.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Modul2Test_Branislav_Jovanovic.Controllers
{
    public class KategorijaController : Controller
    {
        private IKategorijaRepository kategorijaRepo = new KategorijaRepo();
        // GET: Kategorija
        public ActionResult Index()
        {
            var kategorije = kategorijaRepo.GetAll();

            return View(kategorije);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Kategorija kategorija)
        {
            if (!ModelState.IsValid)
            {
                return View(kategorija);
            }
            if (kategorijaRepo.Create(kategorija))
            {
                return RedirectToAction("Index");
            }

            return View("Error");
        }

        public ActionResult Delete(int id)
        {
            try
            {
                kategorijaRepo.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            Kategorija kategorija = new Kategorija();
            kategorija = kategorijaRepo.GetById(id);

            return View(kategorija);
        }

        [HttpPost]
        public ActionResult Edit(int id, Kategorija kategorija)
        {
            try
            {
                kategorijaRepo.Update(kategorija);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }
    }
}