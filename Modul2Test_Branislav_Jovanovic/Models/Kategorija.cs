﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Modul2Test_Branislav_Jovanovic.Models
{
    public class Kategorija
    {
        public int Id { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2)]
        public string Naziv { get; set; }
        [Required]
        [StringLength(40, MinimumLength = 4)]
        public string Opis { get; set; }
    }
}