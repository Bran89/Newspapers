﻿using System.Web;
using System.Web.Mvc;

namespace Modul2Test_Branislav_Jovanovic
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
