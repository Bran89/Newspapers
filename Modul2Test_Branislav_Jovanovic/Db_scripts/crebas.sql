/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2008                    */
/* Created on:     11.02.2018 16:26:06                          */
/*==============================================================*/


if exists (select 1
   from sys.sysreferences r join sys.sysobjects o on (o.id = r.constid and o.type = 'F')
   where r.fkeyid = object_id('CLANAK') and o.name = 'FK_CLANAK_KAT_CLANKA')
alter table CLANAK
   drop constraint FK_CLANAK_KAT_CLANKA
go

if exists (select 1
            from  sysindexes
           where  id    = object_id('CLANAK')
            and   name  = 'CLANAK_KATEGORIJA_CLANKA_FK'
            and   indid > 0
            and   indid < 255)
   drop index CLANAK.CLANAK_KATEGORIJA_CLANKA_FK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('CLANAK')
            and   type = 'U')
   drop table CLANAK
go

if exists (select 1
            from  sysobjects
           where  id = object_id('KATEGORIJA_CLANKA')
            and   type = 'U')
   drop table KATEGORIJA_CLANKA
go

/*==============================================================*/
/* Table: CLANAK                                                  */
/*==============================================================*/
create table CLANAK (
   CLANAK_ID            int                	 identity,
   KATEGORIJA_CLANKA_ID int                  not null,
   NASLOV               varchar(100)          not null,
   AUTOR                varchar(100)          not null,
   TEKST                varchar(1024)         not null,
   constraint PK_CLANAK primary key nonclustered (CLANAK_ID)
)
go

/*==============================================================*/
/* Index: CLANAK_KATEGORIJA_CLANKA_FK                                    */
/*==============================================================*/
create index CLANAK_KATEGORIJA_CLANKA_FK on CLANAK (
KATEGORIJA_CLANKA_ID ASC
)
go

/*==============================================================*/
/* Table: KATEGORIJA_CLANKA                                            */
/*==============================================================*/
create table KATEGORIJA_CLANKA (
   KATEGORIJA_CLANKA_ID        int                  identity,
   NAZIV     				   varchar(100)         null,
   OPIS						   varchar(100)			null,
   constraint PK_KATEGORIJA_CLANKA primary key nonclustered (KATEGORIJA_CLANKA_ID)
)
go

alter table CLANAK
   add constraint FK_CLANAK_KAT_CLANKA foreign key (KATEGORIJA_CLANKA_ID)
      references KATEGORIJA_CLANKA (KATEGORIJA_CLANKA_ID)
go

alter table dbo.CLANAK  with NOCHECK add constraint DEL_CLANAK_KAT_CLANKA foreign key(KATEGORIJA_CLANKA_ID)
references dbo.KATEGORIJA_CLANKA (KATEGORIJA_CLANKA_ID)
on delete cascade
go
