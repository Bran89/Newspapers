﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Modul2Test_Branislav_Jovanovic.Models
{
    public class Clanak
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string Naslov { get; set; }
        [Required]
        [StringLength(1024, MinimumLength = 4)]
        public string Tekst { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 1)]
        public string Autor { get; set; }
        public Kategorija Kategorija { get; set; }

    }
}