﻿using Modul2Test_Branislav_Jovanovic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul2Test_Branislav_Jovanovic.Repository.Interfaces
{
    public interface IClanakRepository
    {
        IEnumerable<Clanak> GetAll();
        Clanak GetById(int id);
        bool Create(Clanak clanak);
        void Update(Clanak clanak);
        void Delete(int id);
    }
}
