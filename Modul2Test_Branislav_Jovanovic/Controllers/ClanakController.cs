﻿using Modul2Test_Branislav_Jovanovic.Models;
using Modul2Test_Branislav_Jovanovic.Repository;
using Modul2Test_Branislav_Jovanovic.Repository.Interfaces;
using Modul2Test_Branislav_Jovanovic.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Modul2Test_Branislav_Jovanovic.Controllers
{
    public class ClanakController : Controller
    {
        private IClanakRepository clanakRepo = new ClanakRepo();
        private IKategorijaRepository kategorijaRepo = new KategorijaRepo();
        // GET: Clanak
        public ActionResult Index()
        {
            ClanakKategorijaViewModel clanakKategorije = new ClanakKategorijaViewModel
            {
                Kategorije = kategorijaRepo.GetAll().ToList(),
                Clanci = clanakRepo.GetAll().ToList()
            };

            return View(clanakKategorije);
        }

        [HttpPost]
        public ActionResult Create(Clanak clanak)
        {
            ModelState.Values.ElementAt(4).Errors.Clear();      // Brisanje gresaka napravljenih tokom dodatne serverske validacije za klasu Kategorija
            ModelState.Values.ElementAt(5).Errors.Clear();
            if (!ModelState.IsValid)
            {
                ClanakKategorijaViewModel clanakKategorije = new ClanakKategorijaViewModel
                {
                    Clanak = clanak,
                    Kategorije = kategorijaRepo.GetAll().ToList(),
                    Clanci = clanakRepo.GetAll().ToList()
                };
                return View("Index", clanakKategorije);
            }

            if (clanakRepo.Create(clanak))
            {
                return RedirectToAction("Index");
            }

            return View("Error");
        }

        public ActionResult Delete(int id)
        {
            try
            {
                clanakRepo.Delete(id);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }
        }

        public ActionResult Edit(int id)
        {
            ClanakKategorijaViewModel clanakKategorije = new ClanakKategorijaViewModel
            {
                Kategorije = kategorijaRepo.GetAll().ToList(),
                Clanak = clanakRepo.GetById(id)
            };

            return View(clanakKategorije);
        }

        [HttpPost]
        public ActionResult Edit(int id, Clanak clanak)
        {
            if (!ModelState.IsValid)
            {
                ClanakKategorijaViewModel clanakKategorije = new ClanakKategorijaViewModel
                {
                    Clanak = clanak,
                    Kategorije = kategorijaRepo.GetAll().ToList(),
                    Clanci = clanakRepo.GetAll().ToList()
                };
                return View(clanakKategorije);
            }

            try
            {
                clanakRepo.Update(clanak);

                return RedirectToAction("Index");
            }
            catch
            {
                return View("Error");
            }

        }
    }
}