﻿using Modul2Test_Branislav_Jovanovic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modul2Test_Branislav_Jovanovic.Repository.Interfaces
{
    public interface IKategorijaRepository
    {
        IEnumerable<Kategorija> GetAll();
        Kategorija GetById(int id);
        bool Create(Kategorija kategorija);
        void Update(Kategorija kategorija);
        void Delete(int id);
    }
}
