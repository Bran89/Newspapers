﻿using Modul2Test_Branislav_Jovanovic.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Modul2Test_Branislav_Jovanovic.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Modul2Test_Branislav_Jovanovic.Repository
{
    public class KategorijaRepo : IKategorijaRepository
    {
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AlephDbContext"].ToString();
            con = new SqlConnection(constr);
        }

        public bool Create(Kategorija kategorija)
        {
            try
            {
                string query = "INSERT INTO Kategorija_Clanka (Naziv, Opis) VALUES (@Naziv, @Opis);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Naziv", kategorija.Naziv);
                    cmd.Parameters.AddWithValue("@Opis", kategorija.Opis);

                    con.Open();
                    var newFormedId = cmd.ExecuteScalar();              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();

                    if (newFormedId != null)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska prilikom upisa nove kategorije. " + ex.StackTrace);
                throw ex;
            }
        }

        public void Delete(int id)
        {
            try
            {
                string query = "DELETE FROM Kategorija_Clanka WHERE Kategorija_Clanka_Id = @Kategorija_Clanka_Id;";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Kategorija_Clanka_Id", id);

                    con.Open();
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom brisanja kategorije. {ex.StackTrace}");
                throw ex;
            }
        }

        public IEnumerable<Kategorija> GetAll()
        {
            string query = "SELECT * FROM Kategorija_Clanka;";
            Connection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;
                con.Open();

                dadapter.Fill(ds, "Kategorija_Clanka");
                dt = ds.Tables["Kategorija_Clanka"];
                con.Close();
            }

            List<Kategorija> kategorije = new List<Kategorija>();

            foreach (DataRow dataRow in dt.Rows)
            {
                int kategorijaId = int.Parse(dataRow["Kategorija_Clanka_Id"].ToString());
                string kategorijaNaziv = dataRow["Naziv"].ToString();
                string kategorijaOpis = dataRow["Opis"].ToString();

                kategorije.Add(new Kategorija() { Id = kategorijaId, Naziv = kategorijaNaziv, Opis = kategorijaOpis });
            }

            return kategorije;
        }

        public Kategorija GetById(int id)
        {
            Kategorija kategorija = null;

            try
            {
                string query = "SELECT * FROM Kategorija_Clanka k WHERE k.Kategorija_Clanka_Id = @Kategorija_Clanka_Id;";
                Connection();

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Kategorija_Clanka_Id", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    con.Open();
                    dadapter.Fill(ds, "Kategorija_Clanka");
                    dt = ds.Tables["Kategorija_Clanka"];
                    con.Close();
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int kategorijaId = int.Parse(dataRow["Kategorija_Clanka_Id"].ToString());
                    string kategorijaNaziv = dataRow["Naziv"].ToString();
                    string kategorijaOpis = dataRow["Opis"].ToString();

                    kategorija = new Kategorija() { Id = kategorijaId, Naziv = kategorijaNaziv, Opis = kategorijaOpis};
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja motora sa id-om: {kategorija.Id}. {ex.StackTrace}");
                throw ex;
            }

            return kategorija;
        }

        public void Update(Kategorija kategorija)
        {
            try
            {
                string query = "UPDATE Kategorija_Clanka SET Naziv = @Naziv, Opis = @Opis WHERE Kategorija_Clanka_Id = @Kategorija_Clanka_Id;";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Kategorija_Clanka_Id", kategorija.Id);
                    cmd.Parameters.AddWithValue("@Naziv", kategorija.Naziv);
                    cmd.Parameters.AddWithValue("@Opis", kategorija.Opis);

                    con.Open();
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom menjanja kategorije. {ex.StackTrace}");
                throw ex;
            }
        }
    }
}