﻿using Modul2Test_Branislav_Jovanovic.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Modul2Test_Branislav_Jovanovic.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Modul2Test_Branislav_Jovanovic.Repository
{
    public class ClanakRepo : IClanakRepository
    {
        private SqlConnection con;
        private void Connection()
        {
            string constr = ConfigurationManager.ConnectionStrings["AlephDbContext"].ToString();
            con = new SqlConnection(constr);
        }

        public bool Create(Clanak clanak)
        {
            try
            {
                string query = "INSERT INTO Clanak (Naslov, Autor, Tekst, Kategorija_Clanka_Id) VALUES (@Naslov, @Autor, @Tekst, @Kategorija_Clanka_Id);";
                query += " SELECT SCOPE_IDENTITY()";        // selektuj id novododatog zapisa nakon upisa u bazu

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Naslov", clanak.Naslov);
                    cmd.Parameters.AddWithValue("@Autor", clanak.Autor);
                    cmd.Parameters.AddWithValue("@Tekst", clanak.Tekst);
                    cmd.Parameters.AddWithValue("@Kategorija_Clanka_Id", clanak.Kategorija.Id);

                    con.Open();
                    var newFormedId = cmd.ExecuteScalar();
                    con.Close();

                    if (newFormedId != null)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska prilikom upisa novog clanka. " + ex.StackTrace);
                throw ex;
            }
        }

        public void Delete(int id)
        {
            try
            {
                string query = "DELETE FROM Clanak WHERE Clanak_Id = @Clanak_Id;";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Clanak_Id", id);

                    con.Open();
                    cmd.ExecuteNonQuery();                              // izvrsi upit nad bazom, vraca id novododatog zapisa
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom clanka. {ex.StackTrace}");
                throw ex;
            }
        }

        public IEnumerable<Clanak> GetAll()
        {
            string query = "SELECT * FROM Clanak LEFT JOIN Kategorija_Clanka ON Clanak.Kategorija_Clanka_Id = Kategorija_Clanka.Kategorija_Clanka_Id;";
            Connection();

            DataTable dt = new DataTable();
            DataSet ds = new DataSet();

            using (SqlCommand cmd = con.CreateCommand())
            {
                cmd.CommandText = query;

                SqlDataAdapter dadapter = new SqlDataAdapter();
                dadapter.SelectCommand = cmd;
                con.Open();

                dadapter.Fill(ds, "Clanak");
                dt = ds.Tables["Clanak"];
                con.Close();
            }

            List<Clanak> clanci = new List<Clanak>();

            foreach (DataRow dataRow in dt.Rows)
            {
                int clanakId = int.Parse(dataRow["Clanak_Id"].ToString());
                string clanakNaslov = dataRow["Naslov"].ToString();
                string clanakAutor = dataRow["AUTOR"].ToString();
                string clanakTekst = dataRow["TEKST"].ToString();

                int kategorijaId = int.Parse(dataRow["Kategorija_Clanka_Id"].ToString());
                string kategorijaNaziv = dataRow["Naziv"].ToString();
                string kategorijaOpis = dataRow["Opis"].ToString();


                Kategorija kategorija = new Kategorija()
                {
                    Id = kategorijaId,
                    Naziv = kategorijaNaziv,
                    Opis = kategorijaOpis
                };

                clanci.Add(new Clanak() { Id = clanakId, Autor = clanakAutor, Naslov = clanakNaslov, Tekst = clanakTekst, Kategorija = kategorija });
            }

            return clanci;
        }

        public Clanak GetById(int id)
        {
            Clanak clanak = null;

            try
            {
                string query = "SELECT * FROM Clanak c LEFT JOIN Kategorija_Clanka ON c.Kategorija_Clanka_Id = Kategorija_Clanka.Kategorija_Clanka_Id WHERE c.Clanak_Id = @Clanak_Id;";
                Connection();

                DataTable dt = new DataTable();
                DataSet ds = new DataSet();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Clanak_Id", id);

                    SqlDataAdapter dadapter = new SqlDataAdapter();
                    dadapter.SelectCommand = cmd;

                    con.Open();
                    dadapter.Fill(ds, "Clanak");
                    dt = ds.Tables["Clanak"];
                    con.Close();
                }

                foreach (DataRow dataRow in dt.Rows)
                {
                    int clanakId = int.Parse(dataRow["Clanak_Id"].ToString());
                    string clanakNaslov = dataRow["Naslov"].ToString();
                    string clanakAutor = dataRow["AUTOR"].ToString();
                    string clanakTekst = dataRow["TEKST"].ToString();

                    int kategorijaId = int.Parse(dataRow["Kategorija_Clanka_Id"].ToString());
                    string kategorijaNaziv = dataRow["Naziv"].ToString();
                    string kategorijaOpis = dataRow["Opis"].ToString();

                    Kategorija kategorija = new Kategorija()
                    {
                        Id = kategorijaId,
                        Naziv = kategorijaNaziv,
                        Opis = kategorijaOpis
                    };

                    clanak = new Clanak() { Id = clanakId, Autor = clanakAutor, Naslov = clanakNaslov, Tekst = clanakTekst, Kategorija = kategorija };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Dogodila se greska prilikom preuzimanja clanka sa id-om: {clanak.Id}. {ex.StackTrace}");
                throw ex;
            }

            return clanak;
        }

        public void Update(Clanak clanak)
        {
            try
            {
                string query = "UPDATE Clanak SET Naslov = @Naslov, Autor = @Autor, Tekst = @Tekst, Kategorija_Clanka_Id = @Kategorija_Clanka_Id WHERE Clanak_Id = @Clanak_Id;";

                Connection();

                using (SqlCommand cmd = con.CreateCommand())
                {
                    cmd.CommandText = query;
                    cmd.Parameters.AddWithValue("@Naslov", clanak.Naslov);
                    cmd.Parameters.AddWithValue("@Autor", clanak.Autor);
                    cmd.Parameters.AddWithValue("@Tekst", clanak.Tekst);
                    cmd.Parameters.AddWithValue("@Kategorija_Clanka_Id", clanak.Kategorija.Id);
                    cmd.Parameters.AddWithValue("@Clanak_Id", clanak.Id);



                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Dogodila se greska prilikom izmena clanka. " + ex.StackTrace);
                throw ex;
            }
        }
    }
}