window.onload = function () {

    document.querySelector("#Clanak_Naslov").onblur = function () {
        var naslov = document.querySelector("#Clanak_Naslov").value;
        validateNaslov(naslov);
    };

    document.querySelector("#Clanak_Naslov").onfocus = function () {
        document.querySelector("#spnaslov").innerHTML = "";
    };

    document.querySelector("#Clanak_Autor").onblur = function () {
        var autor = document.querySelector("#Clanak_Autor").value;
        validateAutor(autor);
    };

    document.querySelector("#Clanak_Autor").onfocus = function () {
        document.querySelector("#spautor").innerHTML = "";
    };
    document.querySelector("#Clanak_Tekst").onblur = function () {
        var tekst = document.querySelector("#Clanak_Tekst").value;
        validateTekst(tekst);
    };

    document.querySelector("#Clanak_Tekst").onfocus = function () {
        document.querySelector("#sptekst").innerHTML = "";
    };
}

function isFormValid(form) {
    var naslov = form["Clanak.Naslov"].value;
    var autor = form["Clanak.Autor"].value;
    var tekst = form["Clanak.Tekst"].value;

    var isNaslovValid = validateNaslov(naslov);
    var isAutorValid = validateAutor(autor);
    var isTekstValid = validateTekst(tekst);

    if (!isNaslovValid || !isAutorValid || !isTekstValid) {
        return false;
    }

    return true;
}

function validateNaslov(naslov) {
    var isValid = true;

    if (!naslov) {
        document.querySelector("#spnaslov").innerHTML = "Niste uneli naslov!";
        isValid = false;
    }
    else if (naslov.length < 1) {
        document.querySelector("#spnaslov").innerHTML = "Naslov ne mo�e da ima manje od jednog karaktera!";
        isValid = false;
    }
    else if (naslov.length > 50) {
        document.querySelector("#spnaslov").innerHTML = "Naslov ne mo�e da ima vi�e od 50 karaktera!";
        isValid = false;
    }
    else {
        document.querySelector("#spnaslov").innerHTML = "";
    }

    return isValid;
}

function validateAutor(autor) {
    var isValid = true;

    if (!autor) {
        document.querySelector("#spautor").innerHTML = "Niste uneli autora!";
        isValid = false;
    }
    else if (autor.length < 1) {
        document.querySelector("#spautor").innerHTML = "Autor ne mo�e da ima manje od jednog karaktera!";
        isValid = false;
    }
    else if (autor.length > 20) {
        document.querySelector("#spautor").innerHTML = "Autor ne mo�e da ima vi�e od 20 karaktera!";
        isValid = false;
    }
    else {
        document.querySelector("#spautor").innerHTML = "";
    }

    return isValid;
}

function validateTekst(tekst) {
    var isValid = true;

    if (!tekst) {
        document.querySelector("#sptekst").innerHTML = "Niste uneli tekst!";
        isValid = false;
    }
    else if (tekst.length < 4) {
        document.querySelector("#sptekst").innerHTML = "Tekst ne mo�e da ima manje od 4 karaktera!";
        isValid = false;
    }
    else if (tekst.length > 1024) {
        document.querySelector("#sptekst").innerHTML = "Tekst ne mo�e da ima vi�e od 1024 karaktera!";
        isValid = false;
    }
    else {
        document.querySelector("#sptekst").innerHTML = "";
    }

    return isValid;
}