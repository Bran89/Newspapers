﻿using Modul2Test_Branislav_Jovanovic.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Modul2Test_Branislav_Jovanovic.ViewModels
{
    public class ClanakKategorijaViewModel
    {
        public Clanak Clanak { get; set; }
        public List<Clanak> Clanci { get; set; }
        public List<Kategorija> Kategorije { get; set; }
    }
}